﻿using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine.UI;
using TMPro;

public class CustomMatchmakingLobbyController : MonoBehaviourPunCallbacks
{
    [SerializeField]
    private GameObject lobbyConnectButton; // button used for joining a lobby
    [SerializeField]
    private GameObject lobbyPanel;  // panel for displaying lobby
    [SerializeField]
    private GameObject mainPanel; // panel for displaying the main menu

    public TMP_InputField playerNameInput; // Input field so player can change their Nickname
    private string roomName; // string for saving room name
    private int roomSize; // int for saving room size

    private List<RoomInfo> roomListings; // list of current rooms
    [SerializeField]
    private Transform roomsContainer; // container for holding all the room listings
    [SerializeField]
    private GameObject roomListingPrefab; // prefab  for displaying each room in the looby

    public override void OnConnectedToMaster() //Callback function for when the first connection is established
    {
        PhotonNetwork.AutomaticallySyncScene = true; // Makes it so whatever scene the master client is having will be loaded in all clients
        lobbyConnectButton.SetActive(true); // activate button for connecting to lobby
        roomListings = new List<RoomInfo>(); // initializing roomListings

        //check for player name saved  to player prefs
        if (PlayerPrefs.HasKey("NickName"))
        {
            if (PlayerPrefs.GetString("NickName") == "")
            {
                PhotonNetwork.NickName = "Player " + Random.Range(0, 1000); // random player name when found
            }
            else
            {
                PhotonNetwork.NickName = PlayerPrefs.GetString("NickName"); // get saved player name
            }
        }
        else
        {
            PhotonNetwork.NickName = "Player " + Random.Range(0, 1000);
        }
        playerNameInput.text = PhotonNetwork.NickName; // update input field with player name
    }

    public void PlayerNameUpdate(string nameInput) // input function for player name
    {
        PhotonNetwork.NickName = nameInput;
        PlayerPrefs.SetString("NickName", nameInput);
    }

    public void JoinLobbyOnClick() // Paired to delay Start button
    {
        mainPanel.SetActive(false);
        lobbyPanel.SetActive(true);
        PhotonNetwork.JoinLobby(); // first tries to join an existing room
    }

    public override void OnRoomListUpdate(List<RoomInfo> roomList) 
    {
        int tempIndex;
        foreach(RoomInfo room in roomList) // loop through each room in room list
        {
            if(roomListings != null) // try to find existing room listings
            {
                tempIndex = roomListings.FindIndex(ByName(room.Name));
            }
            else
            {
                tempIndex = -1;
            }
            if (tempIndex != -1) // remove listing because it has been closed
            {
                roomListings.RemoveAt(tempIndex);
                Destroy(roomsContainer.GetChild(tempIndex).gameObject);
            }
            if(room.PlayerCount > 0) // add room listing because it is new
            {
                roomListings.Add(room);
                ListRoom(room);
            }

        }
       
    }

    static System.Predicate<RoomInfo> ByName(string name) // predicate function for search through room
    {
        return delegate (RoomInfo room)
        {
            return room.Name == name;
        };
    }

    void ListRoom(RoomInfo room)
    {
        if(room.IsOpen && room.IsVisible)
        {
            GameObject tempListing = Instantiate(roomListingPrefab, roomsContainer);
            RoomButton tempButton = tempListing.GetComponent<RoomButton>();
            tempButton.SetRoom(room.Name, room.MaxPlayers, room.PlayerCount);
        }
    }

    public void OnRoomNameChanged(string nameIn)
    {
        roomName = nameIn;
    }
    public void OnRoomSizeChanged(string sizeIn)
    {
        roomSize = int.Parse(sizeIn);
    }

    public void CreateRoom()
    {
        Debug.Log("Creating room now");
        RoomOptions roomOps = new RoomOptions() { IsVisible = true, IsOpen = true, MaxPlayers = (byte)roomSize };
        PhotonNetwork.CreateRoom(roomName, roomOps); // attempting to create a new room
    }

    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        Debug.Log("Tried to create a new room but failed, there must already be a room with the same name");
    }

    public void MatchmakingCancel()
    {
        mainPanel.SetActive(true);
        lobbyPanel.SetActive(false);
        PhotonNetwork.LeaveLobby();
    }

}
