﻿using Photon.Pun;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class RoomButton : MonoBehaviour
{
    [SerializeField]
    private TMP_Text nameText; // display for room name
    [SerializeField]
    private TMP_Text sizeText; // display for room size

    private string roomName; // string for saving room name
    private int roomSize; // int for saving room size
    private int playerCount;

    public void JoinRoomOnClick()
    {
        PhotonNetwork.JoinRoom(roomName);
    }

    public void SetRoom(string nameInput, int sizeInput, int countInput)
    {
        roomName = nameInput;
        roomSize = sizeInput;
        playerCount = countInput;
        nameText.text = nameInput;
        sizeText.text = countInput + "/" + sizeInput;
    }

}
