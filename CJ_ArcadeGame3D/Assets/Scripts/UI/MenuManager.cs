﻿
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Cinemachine;
using CJArcade.SceneManagement;

public class MenuManager : MonoBehaviour
{
    [Header("UI")]
    public RectTransform mainMenu, appearancePanel;

    [Header("GameObjects to assign")]
    public Animator dollyCameraAnimator;
    CinemachineVirtualCamera c_VirtualCam;
    [SerializeField] Transform targetCloths;
    [SerializeField] Transform targetHead;
    [SerializeField] CinemachinePath m_path;
    [SerializeField] CinemachinePathBase trackCloths;
    [SerializeField] CinemachinePathBase trackHead;
    [SerializeField] GameObject customMatchmakingMenu, mainCanvas, appearanceCanvas;
    

    // Start is called before the first frame update
    void Start()
    {
        mainMenu.DOAnchorPos(Vector2.zero, 0.25f);
        c_VirtualCam = FindObjectOfType<CinemachineVirtualCamera>();
        m_path = FindObjectOfType<CinemachinePath>();
    }

    public void AppearanceButton()
    {
        c_VirtualCam.m_LookAt = targetHead.transform;
        mainMenu.DOAnchorPos(new Vector2(-1931, 0), 0.5f);
        appearancePanel.DOAnchorPos(new Vector2(0, 0), 0.5f);
        dollyCameraAnimator.SetBool("isAppearance", true);
    }
    public void AppearanceBackButton()
    {
        mainMenu.DOAnchorPos(Vector2.zero, 0.25f);
        appearancePanel.DOAnchorPos(new Vector2(1931, 0), 0.25f);
        dollyCameraAnimator.SetBool("isAppearance", false);
    }

    public void ClothsButton()
    {
        c_VirtualCam.LookAt = targetCloths.transform;
        dollyCameraAnimator.SetBool("isAppearance", true);
    }

    public void ViewCustomMatchmakingMenu()
    {
        customMatchmakingMenu.SetActive(true);
        mainCanvas.SetActive(false);
        appearanceCanvas.SetActive(false);
    }

}
