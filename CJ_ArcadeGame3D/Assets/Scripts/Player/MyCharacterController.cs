﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ECM.Controllers;
using UnityEngine.UI;
using Mirror;

public class MyCharacterController : BaseCharacterController 
{
    // Start is called before the first frame update
    float forwardAmount;
    [Header("Settings for network")]
    public bool useNetwork;
    public bool useJoyStick;
    private Joystick joy;

    List<string> animlist = new List<string>(new string[] { "Attack1", "Attack2", "Attack3" });
    [Header("Attacks")]
    public bool isAttacking;
    public int combonum;
    public float reset;
    public float resettime;
    public Button attackButton;
    private bool click;

    [SerializeField] private bool isRolling = false;
    [SerializeField] private float inMidAir;
    [SerializeField] private AnimatorOverrideController[] animatorOverride;
 
    void Start()
    {
        if (useNetwork)
        {
            if (GetComponent<NetworkIdentity>().hasAuthority)
            {
                joy = FindObjectOfType<Joystick>();
                attackButton = GameObject.Find("AttackButton").GetComponent<Button>();
            }
        }
        attackButton.onClick.AddListener(callButtonco);
    }

    public override void Update()
    {
        base.Update();
        Attack();
    }

    public override void FixedUpdate()
    {
        Pause();

        if (isPaused)
            return;

        Move();

        Crouch();
    }

    protected override void Animate()
    {
        // If there is no animator, return

        if (animator == null)
            return;

        // Compute move vector in local space

        var move = transform.InverseTransformDirection(moveDirection);

        // Update the animator parameters

        var forwardAmount = move.z;

        animator.SetFloat("Forward", forwardAmount, 0.1f, Time.deltaTime);
        //animator.SetFloat("Turn", Mathf.Atan2(move.x, move.z), 0.1f, Time.deltaTime);

        if (!movement.isGrounded)
            animator.SetFloat("Jump", movement.velocity.y, 0.1f, Time.deltaTime);

        animator.SetBool("OnGround", movement.isGrounded);

        //Setting a rolling parameter in the animator
        if(isRolling)
            animator.SetBool("Rolling", true);
        else
            animator.SetBool("Rolling", false);

        //Setting bool for second jump animation
        if(inMidAir > 0)
            animator.SetFloat("MidAir", inMidAir);
        else
            animator.SetFloat("MidAir", 0);

    }
    //*********Animator Override**********//
    public void SetAnimator(AnimatorOverrideController animatorOverrideController)
    {
        animator.runtimeAnimatorController = animatorOverrideController;
    }
    public void SetAttackType(int value)
    {
        SetAnimator(animatorOverride[value]);   
    }

    protected override void HandleInput()
    {
        //Getting input key for rolling
        
        if (useNetwork)
            if (!GetComponent<NetworkIdentity>().hasAuthority)
                return;
        if (!useJoyStick)
        {
            base.HandleInput();
            if (Input.GetKeyDown(KeyCode.LeftShift))
            {
                isRolling = true;
                StartCoroutine(WaitForRoll());
            }
            else
            {
                isRolling = false;
            }
        }
        else
        {

            if (Input.GetKeyDown(KeyCode.P))
                pause = !pause;


            moveDirection = new Vector3
            {
                x = joy.Horizontal,
                y = 0.0f,
                z = joy.Vertical
            };

            jump = Input.GetButton("Jump");

            crouch = Input.GetKey(KeyCode.C);
            if (Input.GetKeyDown(KeyCode.LeftShift))
            {
                isRolling = true;
                StartCoroutine(WaitForRoll());
            }
            else
            {
                isRolling = false;
            }
        }


    }
    IEnumerator WaitForRoll()
    {
        movement.SetCapsuleHeight(0.9f);
        yield return new WaitForSeconds(0.5f);
        movement.SetCapsuleHeight(1.8f);
    }

    protected override void MidAirJump()
    {
        base.MidAirJump();
        // Checking for mid air jump and set bool for second jump animation
        if(_midAirJumpCount >0)
            inMidAir = _midAirJumpCount;
        else
            inMidAir = 0;
    }


    #region Attack Region

    // Attack() to be called in update
    public void Attack()
    {
        if ((click) && combonum < 3)
        {
            isAttacking = true;
            animator.SetTrigger(animlist[combonum]);
            combonum++;
            reset = 0f;
        }
        if (combonum > 0)
        {
            reset += Time.deltaTime;
            if (reset > resettime)
            {
                isAttacking = false;
                animator.SetTrigger("Reset");
                combonum = 0;
                for (int i = 0; i < animlist.Count - 1; i++)
                {
                    animator.ResetTrigger(animlist[i]);
                }
            }
        }
        if (combonum == 3)
        {
            resettime = 2f;
            combonum = 0;
            isAttacking = false;
        }
        else
        {
            resettime = 1f;
        }
    }

    public void callButtonco()
    {
        StartCoroutine(buttonclickAsmouseclick());
    }

    IEnumerator buttonclickAsmouseclick()
    {
        click = true;
        yield return new WaitForEndOfFrame();
        click = false;
    }
    #endregion

    protected override void Move()
    {
        if (!isAttacking)
            base.Move();
        else
        {
            movement.Move(Vector3.zero, 0, true);
        }
    }

}
