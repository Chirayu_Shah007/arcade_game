﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TopDownCamera : MonoBehaviour
{
    #region Variables
    [SerializeField] public Transform m_Target;
    [SerializeField] float m_Height = 10f;
    [SerializeField] float m_Distance = 20f;
    [SerializeField] float m_Angle = 45f;
    [SerializeField] float m_CameraSmoothSpeed = 0.5f;

    private Vector3 refVelocity;
    #endregion
    // Start is called before the first frame update
    void Start()
    {
        HandleCamera();
    }

    // Update is called once per frame
    void Update()
    {
        HandleCamera();
    }

    protected virtual void HandleCamera()
    {
        if(!m_Target)
        {
            return;
        }

        //Building world positon vector
        Vector3 worldPosition = (Vector3.forward * -m_Distance) + (Vector3.up * m_Height);
        Debug.DrawLine(m_Target.position, worldPosition, Color.red);

        //Build Rotated vector
        Vector3 rotatedVector = Quaternion.AngleAxis(m_Angle, Vector3.up) * worldPosition;
        Debug.DrawLine(m_Target.position, rotatedVector, Color.green);

        //Move Vector Positon with player in realtime
        Vector3 TargetPosition = m_Target.position;
        TargetPosition.y = 0f;
        Vector3 finalPosition = TargetPosition + rotatedVector;
        Debug.DrawLine(m_Target.position, finalPosition, Color.blue);

        transform.position = Vector3.SmoothDamp(transform.position, finalPosition, ref refVelocity, m_CameraSmoothSpeed);
        transform.LookAt(TargetPosition);
    }
}
